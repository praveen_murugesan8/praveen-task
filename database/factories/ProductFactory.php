<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'short_description' => $faker->paragraph,
        'description' => $faker->paragraph,
        'images' => $faker->text,
        'price' => $faker->randomNumber($nbDigits = 3, $strict = true),
        'status' => 1,

    ];
});
