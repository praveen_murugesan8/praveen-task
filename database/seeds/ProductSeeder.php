<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //insert fake data


        DB::table('products')->truncate();
        DB::table('products')->insert(
            [
                'name' => "iphone 13",
                'short_description' => "latest mobile from Apple",
                'description' => "latest latest mobile from Applelatest mobile from Applelatest mobile from Apple",
                'images' => "images",
                'price' => 13.78,
                'status' => 1,
            ]
        );
        DB::table('products')->insert(
            [
                'name' => "nokia 9",
                'short_description' => "latest Stock Android Phone",
                'description' => "lorem ioaum dole lorem ipsum dolar lorem ioaum dole lorem ipsum dolar lorem ioaum dole lorem ipsum dolar",
                'images' => "images",
                'price' => 54.78,
                'status' => 1
            ]
        );

        DB::table('products')->insert(
            [
                'name' => "Samsung S20",
                'short_description' => "flagship mobile",
                'description' => "lorem ioaum dole lorem ipsum dolar lorem ioaum dole lorem ipsum dolar lorem ioaum dole lorem ipsum dolar",
                'images' => "images",
                'price' => 76.78,
                'status' => 1
            ]
        );

    }
}
