<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('cart', 'HomeController@cart');
Route::post('booking', 'ProductController@booking');

Auth::routes();
Route::group(['middleware' => 'auth'],function()
{
    Route::get('/', 'ProductController@index');
Route::resource('product','ProductController');
Route::get('changeStatus', 'ProductController@changeStatus');
Route::get('order', 'ProductController@order');
Route::get('order/{id}', 'ProductController@orderView');
});




