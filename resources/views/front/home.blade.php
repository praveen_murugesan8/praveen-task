@extends('front.layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="col-lg-12">
                    <div class="row style-category">
                        @foreach ($products as $product)
                            <div class="col-12 col-md-6 col-lg-4">
                                <div class="card card-home">
                                    <img class="card-img-top" src="{{url($product->images)}}"
                                         alt="No Image" height="400" width="400"
                                         onerror="this.onerror=null;this.src='{{url('images/product/default.jpg')}}';">
                                    <div class="card-body">
                                        <h4 class="card-title text-center"><a href="#"
                                                                              title="View Product">{{$product->name}}</a>
                                        </h4>

                                        <div class="row">
                                            <div class="col">
                                                <p class="btn btn-danger btn-block">₹ {{$product->price}}</p>
                                            </div>
                                            <div class="col">
                                                <a href="#" data-id="{{$product->id}}" data-name="{{$product->name}}"
                                                   data-image="{{url($product->images)}}"
                                                   data-price="{{$product->price}}" class="add-to-cart btn btn-primary">Add</a>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @include('front.modal')
        </div>
    </div>
@endsection
@section('js')
    @include('front.script')
@endsection

