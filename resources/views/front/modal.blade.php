<div class="modal fade" id="cart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cart</h5>
                <button id="cart-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="show-cart table">

                </table>
                <div>Total price:₹ <span class="total-cart"></span></div>
            </div>
            <div class="modal-footer">
                <button class="clear-cart btn btn-danger">Clear Cart</button>
                <a class="btn btn-primary" href="{{url('cart') }}" id="checkout">CheckOut</a>
            </div>
        </div>
    </div>
</div>
