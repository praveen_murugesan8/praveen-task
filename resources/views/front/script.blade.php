<script>
    //modal open for booking
    $(document).ready(function () {
        $('.add-to-cart').click(function (event) {
            event.preventDefault();
            var name = $(this).data('name');
            var id = $(this).data('id');
            var price = Number($(this).data('price'));
            var image = $(this).data('image');
            shoppingCart.addItemToCart(id, name, price, 1, image);
            displayCart();
        });
        //get details
        var shoppingCart = (function () {
            cart = [];

            function Item(id, name, price, count, image) {
                this.id = id;
                this.name = name;
                this.price = price;
                this.image = image;
                this.count = count;
            }
            // Save cart
            function saveCart() {
                sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
            }

            // Load cart
            function loadCart() {
                cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
            }
            //session storage
            if (sessionStorage.getItem("shoppingCart") != null) {
                loadCart();
            }

            var obj = {};
            // Add to cart
            obj.addItemToCart = function (id, name, price, count, image) {
                for (var item in cart) {
                    if (cart[item].id === id) {
                        cart[item].count++;
                        saveCart();
                        return;
                    }
                }
                var item = new Item(id, name, price, count, image);
                cart.push(item);
                saveCart();
            }
            // Set count from item
            obj.setCountForItem = function (name, count) {
                for (var i in cart) {
                    if (cart[i].name === name) {
                        cart[i].count = count;
                        break;
                    }
                }
            };
            // Remove item from cart
            obj.removeItemFromCart = function (id) {
                for (var item in cart) {
                    if (cart[item].id === id) {
                        cart[item].count--;
                        if (cart[item].count === 0) {
                            cart.splice(item, 1);
                        }
                        break;
                    }
                }
                saveCart();
            }

            // Remove all items from cart
            obj.removeItemFromCartAll = function (id) {
                for (var item in cart) {
                    if (cart[item].id === id) {
                        cart.splice(item, 1);
                        break;
                    }
                }
                saveCart();
            }

            // Clear cart
            obj.clearCart = function () {
                cart = [];
                saveCart();
            }

            // Count cart
            obj.totalCount = function () {
                var totalCount = 0;
                for (var item in cart) {
                    totalCount += cart[item].count;
                }
                return totalCount;
            }

            // Total cart
            obj.totalCart = function () {
                var totalCart = 0;
                for (var item in cart) {
                    totalCart += cart[item].price * cart[item].count;
                }
                return Number(totalCart.toFixed(2));
            }

            // List cart
            obj.listCart = function () {
                var cartCopy = [];
                for (i in cart) {
                    item = cart[i];
                    itemCopy = {};
                    for (p in item) {
                        itemCopy[p] = item[p];

                    }
                    itemCopy.total = Number(item.price * item.count).toFixed(2);
                    cartCopy.push(itemCopy)
                }
                return cartCopy;
            }
            return obj;
        })();

        // Clear items
        $('.clear-cart').click(function () {
            shoppingCart.clearCart();
            displayCart();
        });


        // Delete item button
        $('.show-cart').on("click", ".delete-item", function (event) {
            var id = $(this).data('id');
            shoppingCart.removeItemFromCartAll(id);
            displayCart();
        })


        // -1 minus icon subtract function
        $('.show-cart').on("click", ".minus-item", function (event) {
            var id = $(this).data('id');
            shoppingCart.removeItemFromCart(id);
            displayCart();
        })

        // +1 plus icon add 1 quantity
        $('.show-cart').on("click", ".plus-item", function (event) {
            var id = $(this).data('id');
            shoppingCart.addItemToCart(id);
            displayCart();
        })

        // Item count input
        $('.show-cart').on("change", ".item-count", function (event) {
            var name = $(this).data('name');
            var count = Number($(this).val());
            shoppingCart.setCountForItem(name, count);
            displayCart();
        });
        displayCart();

        // for assigning value to checkout fields
        function displayCart() {
            var cartArray = shoppingCart.listCart();
            var output = "";
            for (var i in cartArray) {
                var image = cartArray[i].image;
                if (cartArray[i].image == "http://127.0.0.1:8000") {
                    var image = "http://127.0.0.1:8000/images/product/default.jpg";
                }
                output += "<tr>"
                    + "<td>" + cartArray[i].name + "</td>"
                    + "<td><img src='" + image + "' height=50 width=50 ></td>"
                    + "<td>(" + cartArray[i].price + ")</td>"
                    + "<td><div class='input-group'><button class='minus-item input-group-addon btn btn-primary' data-id=" + cartArray[i].id + ">-</button>"
                    + "<input type='number' class='item-count form-control' data-id='" + cartArray[i].id + "' value='" + cartArray[i].count + "'>"
                    + "<button class='plus-item btn btn-primary input-group-addon' data-id=" + cartArray[i].id + ">+</button></div></td>"
                    + "<td><button class='delete-item btn btn-danger' data-id=" + cartArray[i].id + ">X</button></td>"
                    + " = "
                    + "<td>" + cartArray[i].total + "</td>"
                    + "</tr>";
            }
            $('.show-cart').html(output);
            $('.total-cart').html(shoppingCart.totalCart());
            $('.total-count').html(shoppingCart.totalCount());

            if (shoppingCart.totalCount() == 0) {

                $('#checkout').hide();
            } else {
                $('#checkout').show();
            }

        }

        // Delete item button
        $('.show-cart').on("click", ".delete-item", function (event) {
            $(".isloading-overlay").show();
            var id = $(this).data('id');
            shoppingCart.removeItemFromCartAll(id);
            $(".isloading-overlay").delay(800).fadeOut();
            displayCart();
        });

        // storing order details
        $('#confirm_booking').on("click", function (event) {
            //verify booking rules
            var name = $('#name').val();
            var email = $('#email').val();
            var mobile = $('#mobile').val();
            var address = $('#address').val();
            var product_details = shoppingCart.listCart();
            var total_price = shoppingCart.totalCart();
            $.ajax({
                url: "{{ url('booking')}}",
                type: "POST",
                data: {
                    '_token': "{{ csrf_token() }}",
                    'name': name,
                    'email': email,
                    'mobile': mobile,
                    'address': address,
                    'total_price': total_price,
                    'product_details': product_details
                },
                dataType: "json",
                async: false,
                enctype: 'multipart/form-data',
                cache: false,
                success: function (response) {
                    $('#success-msg').html('<p style="background: #5cc88b;color: #000;font-weight: bold;padding: 50px;"> Ordered Successfully</p>');
                    $("#verify_booking")[0].reset();
                    shoppingCart.clearCart();
                    $('.box-grey').hide();
                    $('.show-cart').html('');
                    $('.total-cart').html('');
                    $('.total-count').html(0);

                },
                error: function (response) {
                    console.log("error");
                    $("#signuperrmsg").html("server Error").show();
                }
            });

        });
    });
</script>
