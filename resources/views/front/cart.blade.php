@extends('front.layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <form method="post" role="form" id="verify_booking">
                @csrf
                <div class="row">
                    <div id="success-msg" class="col-md-12"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-row">
                            <div class="col-md-12 form-group">
                                <label class="label2">Name</label>
                                <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="label2">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Email"
                                       data-rule="email"
                                       data-msg="Please enter a valid email" value="">
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="label2">Phone Number</label>
                                <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Phone no"
                                       oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="">
                            </div>
                            <div class="col-md-12 form-group">
                                <label class="label2">Address</label>
                                <textarea class="form-control" name="address" id="address"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 font-weight-bold">

                        <div class="col-md-12 box-grey">
                            <table class="show-cart table">
                            </table>
                            <div>Total price:₹ <span class="total-cart"></span></div>
                        </div>
                    </div>
                    <div class="col-lg-12 font-weight-bold">
                        <div class="text-center">
                            <button id="confirm_booking" type="button">Proceed To Pay</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        @include('front.modal')
    </div>
@endsection
@section('js')
    @include('front.script')
@endsection
