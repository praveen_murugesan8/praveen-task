    <div class="sidebar" data-color="rose" data-background-color="black">
        <div class="sidebar-wrapper">
            <nav>
                <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"   href="{{ url('product') }}" >
                                <i class="material-icons">phone</i>
                                <p>Products
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse"   href="{{ url('order') }}" >
                                <i class="material-icons">phone</i>
                                <p>Orders
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('logout') }}" class="nav-link"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="material-icons">logout</i>
                                <p>Logout
                                </p>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">{{ csrf_field() }}</form>
                        </li>
                </ul>
            </nav>
        </div>
    </div>
