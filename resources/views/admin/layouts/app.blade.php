<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Admin Praveen') }}</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/admin/css/material.css') }}" />
    <link rel="stylesheet" href="{{ asset('/admin/css/font-awesome.min.css') }}">
    <link href="{{ asset('/admin/css/style.css?v=2.1.2') }}" rel="stylesheet" />
    @yield('css')
</head>
<body>
    <div class="wrapper ">
        @include('admin.layouts.header')
            <div class="main-panel">
                @include('admin.layouts.nav')
                <div class="content">
                    @yield('content')
                </div>
            </div>
    </div>
    <script src="{{ asset('/admin/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('/admin/js/core/sweetalert2.js') }}"></script>
    @yield('js')
</body>
</html>
