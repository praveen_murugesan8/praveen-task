@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <form id="subadmin-add" action="{{ url('product') }}" method="POST" role="form"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-error alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="card-header">
                                <h4 class="card-title"> {{ $page_name }}</h4>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name"
                                                   name="name" placeholder="Product Name "
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">

                                                <textarea class="form-control" id="short_description"
                                                          name="short_description"
                                                          placeholder="Short Description"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                                <textarea class="form-control" id="description"
                                                          name="description" placeholder="Description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="price"
                                                   name="price" placeholder="Price " oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <label>upload Image</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="fileinput">
                                            <div>
                                                <input type="file" name="image"
                                                       accept="image/x-png,image/gif,image/jpeg"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" id="btnSubmit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
