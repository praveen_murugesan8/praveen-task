@extends('admin.layouts.app')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary card-header-icon">


                        <h4 class="card-title">{{ $page_name }}</h4>
                        <a class="btn btn-primary pull-right" href="{{ route('product.create') }}">Add Product</a>
                    </div>

                    <div class="card-body">
                        <div class="toolbar">

                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover"
                                   cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Short Description</th>
                                    <th>Price</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $product)

                                    <tr>
                                        <td>{{$product->name}}</td>
                                        <td><img src="{{url($product->images)}}" height="100" width="100" alt="no Image"
                                                 onerror="this.onerror=null;this.src='{{url('images/product/default.jpg')}}';"/>
                                        </td>
                                        <td>{{$product->short_description}}</td>
                                        <td>{{$product->price}}</td>
                                        {{-- <td>
                                            <input data-id="{{$user->id}}" class="toggle-class" type="checkbox"
                                             data-onstyle="success" data-offstyle="danger" data-toggle="toggle"
                                              data-on="Active" data-off="InActive" {{ $product->status ? 'checked' : '' }}
                                              >
                                         </td> --}}
                                        <td>
                                            <div class="togglebutton">
                                                <label>
                                                    <input type="checkbox" class="toggle-class" id="{{$product->id}}"
                                                           @if($product->status == 'active')checked @endif>
                                                    <span class="toggle" id="{{$product->id}}"></span>
                                                </label>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>


@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('.toggle-class').change(function () {
                console.log("here");
                var status = $(this).prop('checked') == true ? 1 : 2;
                var product_id = $(this).prop('id');
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/changeStatus',
                    data: {'status': status, 'product_id': product_id},
                    success: function (data) {
                        console.log(data);
                        swal({
                            title: "Success",
                            text: data.success,
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-success",
                            type: "success"
                        }).catch(swal.noop)
                    }
                });
            })

        });
    </script>
@endsection
