@extends('admin.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <h4 class="card-title">{{ $page_name }}</h4>
                    </div>
                    <div class="card-body">

<div class="row">
    <div class="col-md-4">
        <label>Customer Name</label>
    </div>
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
        <label><b>{{$order->name}}</b></label>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <label>Customer Email</label>
    </div>
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
        <label><b>{{$order->email}}</b></label>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <label>Customer Mobile</label>
    </div>
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
        <label><b>{{$order->mobile}}</b></label>
    </div>
</div>


<div class="row">
    <div class="col-md-4">
        <label>Customer Address</label>
    </div>
    <div class="col-md-2">

    </div>
    <div class="col-md-6">
        <label><b>{{$order->address}}</b></label>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <label>Order Details</label>
    </div>
</div>
<table class="col-md-12">
    <tr>
        <th>No</th>
        <th>Product name</th>
        <th>Quantity</th>
        <th>Price</th>
    </tr>
@php
$product_details = json_decode($order->product_details);
$i=1;
@endphp
    @foreach ( $product_details as $row)
      <tr>
        <td>{{$i}}</td>

          <td>{{$row->name}}</td>
          <td>{{$row->count}}</td>
          <td>{{$row->price}}</td>
      </tr>
      @php
      $i++;
      @endphp
    @endforeach
    <tr>
        <td colspan="3" class="text-right"><b>Total Price</b></td>
        <td>{{$order->total_price}}</td>
    </tr>
</table>


                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
@endsection
@section('js')

