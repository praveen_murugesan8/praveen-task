<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //products list web
    public function index()
    {

        $this->data['products'] = Product::where('status','active')->get();
        return view('front.home',$this->data);
    }

    // foe viewing cart
    public function cart()
    {
        return view('front.cart');
    }
}
