<?php

namespace App\Http\Controllers;

use app\Library\AppHelper;
use App\Product;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    //product list

    public function index()
    {
        $this->data['page_name'] = "Product List";
        $this->data['products'] = Product::all();
        return view('admin.product.index', $this->data);
    }

    // product create

    public function create()
    {
        $this->data['page_name'] = "Create Product";
        return view('admin.product.create', $this->data);
    }

    // storing product

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:products',
            'price' => 'required|numeric',
            'short_description' => 'required',
            'description' => 'required',
            'image' => 'required',
        ]);

        $destinationPath = '';
        $product_image = '';
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $product_image = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('images/product');
            $image->move($destinationPath, $product_image);
        }

        $user = new Product();
        $user->name = $request->name;
        $user->short_description = $request->short_description;
        $user->description = $request->description;
        $user->images = 'images/product/' . $product_image;
        $user->price = $request->price;
        $user->save();
        return redirect()->back()->with([
            'message' => "New product " . $request->name . " was added",
            'm_type' => 'success'
        ]);

    }

    // change product status
    public function changeStatus(Request $request)
    {
        if ($request->status == 1) {
            $message = "Product Enabled.";
        }
        if ($request->status == 2) {
            $message = "Product Disabled.";
        }
        $product = Product::find($request->product_id);
        $product->status = $request->status;
        $product->save();

        return response()->json(['success' => $message]);
    }

    // for storing order details
    public function booking(Request $request)
    {
            $order = new Order();
            $order->name = $request->name;
            $order->email = $request->email;
            $order->mobile = $request->mobile;
            $order->address = $request->address;
            $order->product_details = json_encode($request->product_details);
            $order->total_price = $request->total_price;
            $order->created_at = date("Y-m-d H:i:s");
            $order->save();
            $response = [
                'code' => 200,
                'message' => 'Ordered Successfully',
                'data' => $order
            ];
            return $response;
    }

    // list Order Admin
    public function order(Request $request)
    {
        $this->data['page_name'] = "Order List";
        $this->data['orders'] = Order::all();
        return view('admin.order.index', $this->data);

    }

    // View Order
    public function orderView($id)
    {
        $this->data['page_name'] = "Order View";
        $this->data['order'] = Order::findOrFail($id);
        return view('admin.order.view', $this->data);

    }
}
